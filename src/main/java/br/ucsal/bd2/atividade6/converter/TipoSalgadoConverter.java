package br.ucsal.bd2.atividade6.converter;

import javax.persistence.AttributeConverter;

import br.ucsal.bd2.atividade6.enums.TipoSalgadoEnum;

// Gerar metodos com implements AttributeConverter
public class TipoSalgadoConverter implements AttributeConverter<TipoSalgadoEnum, Integer> {

	public Integer convertToDatabaseColumn(TipoSalgadoEnum tipoSalgadoEnum) {
		return tipoSalgadoEnum != null ? tipoSalgadoEnum.getCodigo() : null;
	}

	public TipoSalgadoEnum convertToEntityAttribute(Integer dbData) {
		return dbData != null ? TipoSalgadoEnum.valueOfCodigo(dbData) : null;
	}

}
