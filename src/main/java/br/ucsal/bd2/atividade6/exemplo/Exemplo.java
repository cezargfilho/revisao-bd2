package br.ucsal.bd2.atividade6.exemplo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

import br.ucsal.bd2.atividade6.domain.Cliente;
import br.ucsal.bd2.atividade6.domain.Endereco;
import br.ucsal.bd2.atividade6.domain.Produto;
import br.ucsal.bd2.atividade6.domain.Salgado;
import br.ucsal.bd2.atividade6.enums.TipoSalgadoEnum;

public class Exemplo {

	private static final Logger logger = Logger.getLogger(Exemplo.class);

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("lanchonete");
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();

			Produto produtoA = new Produto();
			produtoA.setNome("Coxinha");
			BigDecimal bigDecimal = new BigDecimal(4.50);
			produtoA.setValorUnitario(bigDecimal);
			produtoA.setQtdMinima(10);

			em.persist(produtoA);

			Salgado salgadoA = new Salgado();
			salgadoA.setNome("Esfirra");
			salgadoA.setQtdMinima(10);
			BigDecimal bigDecimalEsfirra = new BigDecimal(5.50);
			salgadoA.setValorUnitario(bigDecimalEsfirra);
			salgadoA.setTipoSalgadoEnum(TipoSalgadoEnum.FRITO);

			em.persist(salgadoA);

			Cliente cliente = new Cliente();
			cliente.setCpf("61616161");
			cliente.setNome("Cezar");

			Endereco endereco = new Endereco("Rua da A", "32", "Vila Laura");
			em.persist(endereco);
			
			List<Endereco> enderecos = new ArrayList<Endereco>();
			enderecos.add(endereco);
			cliente.setEnderecos(enderecos);
			
			List<Produto>produtos = new ArrayList<Produto>();
			produtos.add(produtoA);
			produtos.add(salgadoA);
			cliente.setProdutos(produtos);

			em.persist(cliente);

			em.getTransaction().commit();
			em.close();

		} catch (Exception e) {
			logger.error("Erro: ", e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	public void questaoSete() {

	}

}
