package br.ucsal.bd2.atividade6.enums;

public enum TipoSalgadoEnum {

	ASSADO(13), FRITO(23), CONGELADO(34);

	private Integer codigo;

	private TipoSalgadoEnum(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigoEnum(Integer codigo) {
		this.codigo = codigo;
	}

	public static TipoSalgadoEnum valueOfCodigo(Integer codigo) {
		for (TipoSalgadoEnum tipoSalgadoEnum : values()) {
			if (tipoSalgadoEnum.getCodigo().equals(codigo)) {
				return tipoSalgadoEnum;
			}
		}
		throw new IllegalArgumentException("Codigo inválido" + codigo);
	}

}
