package br.ucsal.bd2.atividade6.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity(name = "tab_produto")
@Inheritance(strategy = InheritanceType.JOINED)
public class Produto {

	// Integer codigo - integer (sequence) - not null
	// String nome - varchar(30) - not null
	// Double valorUnitario - numeric(10,2) - not null
	// Integer qtdMinima - integer - not null

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "produto_codigo", columnDefinition = "integer")
	private Integer codigo;
	
	@Column(length = 30, nullable = false)
	private String nome;

	@Column(name = "valor_unitario", columnDefinition = "numeric", precision = 10, scale = 2, nullable = false)
	private BigDecimal valorUnitario;

	@Column(name = "qtd_minima", columnDefinition = "integer", nullable = false)
	private Integer qtdMinima;

	public Produto() {

	}

	public Produto(Integer codigo, String nome, BigDecimal valorUnitario, Integer qtdMinima) {
		this.codigo = codigo;
		this.nome = nome;
		this.valorUnitario = valorUnitario;
		this.qtdMinima = qtdMinima;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public Integer getQtdMinima() {
		return qtdMinima;
	}

	public void setQtdMinima(Integer qtdMinima) {
		this.qtdMinima = qtdMinima;
	}

}
