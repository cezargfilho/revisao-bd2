package br.ucsal.bd2.atividade6.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "tab_endereco")
public class Endereco {

	// Integer codigo - integer (sequence) - not null
	// String rua - varchar(40) - not nullll
	// String numero - varchar(10) - not null
	// String bairro - varchar(40) - not null

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "codigo_endereco" ,columnDefinition = "INTEGER")
	private Integer codigo;

	@Column(length = 40, nullable = false)
	private String rua;

	@Column(length = 10, nullable = false)
	private String numero;

	@Column(length = 40, nullable = false)
	private String bairro;

	public Endereco() {

	}

	public Endereco(String rua, String numero, String bairro) {
		super();
		this.rua = rua;
		this.numero = numero;
		this.bairro = bairro;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

}
