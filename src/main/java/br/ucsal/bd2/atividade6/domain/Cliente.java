package br.ucsal.bd2.atividade6.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity(name = "tab_cliente")
public class Cliente {

	// String cpf - char(11) - not null
	// String nome - char(40) - not null
	// List<Endereco> enderecos
	// List<Produto> produtos

	@Id
	@Column(name = "cliente_codigo", columnDefinition = "char(11)", nullable = false)
	private String cpf;

	@Column(length = 40, nullable = false)
	private String nome;

	@ManyToMany
	@JoinTable(name = "tab_cliente_endereco", joinColumns = @JoinColumn(name = "cliente_codigo"), inverseJoinColumns = @JoinColumn(name = "endereco_codigo"))
	private List<Endereco> enderecos;

	@ManyToMany
	@JoinTable(name = "tab_cliente_produto", joinColumns = @JoinColumn(name = "cliente_codigo"), inverseJoinColumns = @JoinColumn(name = "produto_codigo"))
	private List<Produto> produtos;

	public Cliente() {

	}

	public Cliente(String cpf, String nome, List<Endereco> enderecos, List<Produto> produtos) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.enderecos = enderecos;
		this.produtos = produtos;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

}
