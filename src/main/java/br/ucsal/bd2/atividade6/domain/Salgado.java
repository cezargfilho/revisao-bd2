package br.ucsal.bd2.atividade6.domain;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;

import br.ucsal.bd2.atividade6.converter.TipoSalgadoConverter;
import br.ucsal.bd2.atividade6.enums.TipoSalgadoEnum;

// Utilize a estrategia JOINED para a heranca21

@Entity(name = "tab_salgado")
public class Salgado extends Produto {

	@Convert(converter = TipoSalgadoConverter.class)
	@Column(name = "tipo_salgado", columnDefinition = "integer", nullable = false)
	private TipoSalgadoEnum tipoSalgadoEnum;

	public Salgado() {

	}

	public Salgado(TipoSalgadoEnum tipoSalgadoEnum) {
		super();
		this.tipoSalgadoEnum = tipoSalgadoEnum;
	}



	public TipoSalgadoEnum getTipoSalgadoEnum() {
		return tipoSalgadoEnum;
	}

	public void setTipoSalgadoEnum(TipoSalgadoEnum tipoSalgadoEnum) {
		this.tipoSalgadoEnum = tipoSalgadoEnum;
	}

}
