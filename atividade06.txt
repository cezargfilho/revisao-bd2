UNIVERSIDADE CAT�LICA DO SALVADOR
DISCIPLINA � BANCO DE DADOS 2
PROFESSOR � ANTONIO CLAUDIO P. NEIVA
ANO/SEMESTRE � 2019/1


Exerc�cio 03 - JPA



Assuntos abordados:
Mapeamento Objeto Relacional utilizando JPA
Consultas utilizando JPQL


Orienta��es gerais:
Crie um projeto Java Maven 
Explore os recursos do JPA: constraints, sequences, detalhes de nomenclatura de colunas e tabelas, obrigatoriedade, etc.
Utilize a estrat�gia JOINED para a heran�a;
Regra de forma��o de nomes: 
a) os nomes de campo devem utilizar "_" para concatena��o de palavras. 


Objetivo:

Utilizando JPA, fa�a o mapeamento objeto relacional das seguintes enumera��es, classes e seus atributos:

1. Produto
	Integer codigo	- integer (sequence) - not null
	String nome	- varchar(30)			- not null
	Double valorUnitario	- numeric(10,2)	- not null
	Integer qtdMinima	- integer 		- not null


2. Salgado (subclasse de Produto)
	TipoSalgadoEnum tipo	- not null
	
4. Cliente 
	String cpf	- char(11)	- not null
	String nome	- char(40)	- not null
	List<Endereco> enderecos
	List<Produto> produtos		

5. Endereco
	Integer codigo	- integer (sequence) - not null
	String rua	- varchar(40)	- not null
	String numero	- varchar(10)	- not null
	String bairro	- varchar(40)	- not null

6. TipoSalgadoEnum (enumera��o; deve ser armazenado no banco de dados o c�digo (13 para assado, 23 para frito e 34 para congelado)
	ASSADO (13)
	FRITO (23)
	CONGELADO (34)       

7. Utilizando JPA, fa�a a inclus�o de 2 produto (1 salgado e outro gen�rico) e 2 clientes. (crie um m�todo espec�fico para esta a��o)

8. Utilizando JPA, fa�a a exclus�o de 1 dos clientes. (crie um m�todo espec�fico para esta a��o)

9. Utilizando JPA, fa�a a atualiza��o do nome do cliente que n�o foi exclu�do. (crie um m�todo espec�fico para esta a��o)

10. Utilizando JPA, crie uma consulta que retorne o CPF e o nome dos clientes que n�o possuem nenhum endere�o cadastrado. Utilize um DTO para que sua consulta retorne apenas os dados solicitados e n�o um cliente completo.Seu m�todo deve ter retorno void e listar o DTO gerado (utilize um toString no DTO para facilitar a exibi��o do mesmo).

11. Utilizando JPA, crie uma consulta que retorne os clientes que fizeram pedido do produto com nome �Coxinha�. Seu m�todo deve ter retorno List<Cliente> e n�o deve exibir os dados.
